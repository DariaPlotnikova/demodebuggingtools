from .models import Author, Publication


def publication_to_json(pub: Publication) -> dict:
    """Convert publication to full JSON representation."""
    return dict(
        id=pub.id,
        title=pub.title,
        preview=pub.content[:50],
        created=pub.created,
        visits=pub.visits,
    )


def with_authors_to_json(pub: Publication) -> dict:
    """Converts publication to short representation with authors names."""
    return dict(
        id=pub.id,
        authors=', '. join(str(a) for a in pub.authors.all()),
        title=pub.title,
    )


def author_to_json(author: Author) -> dict:
    """Converts author's data to full JSON representation."""
    return dict(
        id=author.id,
        name=f'{author.surname} {author.initials}',
        pubs_count=author.publications.count(),
        # visits attribute is annotated to queryset in calling code
        total_visits=author.visits or 0,
    )
