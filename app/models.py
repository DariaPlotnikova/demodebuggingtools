from time import sleep

from django.db import models


class Author(models.Model):
    surname = models.CharField(
        verbose_name='Surname',
        max_length=50,
    )
    initials = models.CharField(
        verbose_name='Initials',
        max_length=10,
    )

    def __str__(self):
        return f'{self.initials} {self.surname}'

    def get_visits(self):
        """This is just to demonstrate cProfile features."""
        sleep(0.25)
        # This attribute will be always annotated in examples
        return getattr(self, 'visits') or 0


class Publication(models.Model):
    title = models.CharField(
        verbose_name='Title',
        max_length=255,
    )
    content = models.TextField(
        verbose_name='Content',
    )

    authors = models.ManyToManyField(
        to=Author,
        verbose_name='Authors',
        related_name='publications',
        related_query_name='publications',
    )

    created = models.DateTimeField(
        verbose_name='Published',
        auto_now_add=True,
    )
    visits = models.PositiveBigIntegerField(
        verbose_name='Amount of visits',
        default=0,
    )

    def __str__(self):
        return self.title
