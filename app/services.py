from time import sleep

from django.db.models import Sum
from silk.profiling.profiler import silk_profile

from .models import Author


def do_long_action():
    """Just to demonstrate cProfile features."""
    sleep(2)


@silk_profile('top-authors-db')
def get_authors_db():
    """Returns authors calculating order on DB level."""
    return Author.objects.annotate(
        visits=Sum('publications__visits')
    ).prefetch_related('publications').order_by('-visits')


def get_authors_python():
    """Returns authors calculating order in python side."""
    with silk_profile('top-authors-py'):
        authors = Author.objects.annotate(visits=Sum('publications__visits'))
        do_long_action()
        return list(
            sorted(
                authors,
                key=lambda a: a.get_visits(),
                reverse=True
            )
        )
