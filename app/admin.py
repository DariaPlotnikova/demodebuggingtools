from django.contrib import admin

from .models import Author, Publication


@admin.register(Author)
class AuthorAdmin(admin.ModelAdmin):
    list_display = ('surname', 'initials')


@admin.register(Publication)
class PublicationAdmin(admin.ModelAdmin):
    list_display = ('title', 'created', 'visits')
