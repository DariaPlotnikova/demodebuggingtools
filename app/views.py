from datetime import datetime
from datetime import timedelta

from django.http.request import HttpRequest
from django.http.response import JsonResponse

from .contexts import (
    publication_to_json,
    with_authors_to_json,
    author_to_json,
)
from .models import Publication
from .services import get_authors_db, get_authors_python


def publications_list(request: HttpRequest):
    """Returns list of publication in full representation."""
    last_week_pubs = Publication.objects.all()
    return JsonResponse({
        'objects': [publication_to_json(pub) for pub in last_week_pubs],
        'total': last_week_pubs.count(),
    })


def publications_authors(request: HttpRequest):
    """Returns publications with information about its authors."""
    pubs_with_authors = Publication.objects.all()
    return JsonResponse({
        'objects': [with_authors_to_json(pub) for pub in pubs_with_authors],
        'total': pubs_with_authors.count(),
    })


def top_authors_by_visits(request: HttpRequest, calc_type: str):
    """Returns top authors by visits calculated by calc_type.

    :param request: HTTP request.
    :param calc_type: type to calculate order - in db or in python. Possible
        choices - 'db', 'py'.
    """
    if calc_type == 'db':
        authors = get_authors_db()
    else:
        authors = get_authors_python()

    return JsonResponse({
        'objects': [author_to_json(a) for a in authors],
        'total': len(authors),
    })
