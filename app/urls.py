from django.urls import path
from .views import (
    publications_list,
    publications_authors,
    top_authors_by_visits,
)


urlpatterns = [
    path('', publications_list, name='list'),
    path('with-authors/', publications_authors, name='with-authors'),
    path('top-authors/<str:calc_type>/',
         top_authors_by_visits,
         name='top-authors'),
]
